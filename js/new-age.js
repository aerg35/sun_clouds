(function($) {
  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 48)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNav',
    offset: 54
  });

  // Collapse Navbar
  var navbarCollapse = function() {
    if ($("#mainNav").offset().top > 100) {
      $("#mainNav").addClass("navbar-shrink");
    } else {
      $("#mainNav").removeClass("navbar-shrink");
    }
  };
  // Collapse now if page is not at top
  navbarCollapse();
  // Collapse the navbar when page is scrolled
  $(window).scroll(navbarCollapse);

})(jQuery); // End of use strict

const api = {
  key: "40b5851235068648a6bf761247090b35",
  base: "https://api.openweathermap.org/data/2.5/"
}

window.addEventListener("load", () => {
  let lat;
  let long;
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(position => {
      lat = position.coords.latitude;
      long = position.coords.longitude;
      getLocalResults(lat, long);
    });
  }
});

const searchbox = document.querySelector('.search-box');
searchbox.addEventListener('keypress', setQuery);

function getLocalResults (lat, long) {
  fetch(`${api.base}weather?lat=${lat}&lon=${long}&units=metric&APPID=${api.key}`)
  .then(weather => {
    return weather.json();
  }).then(displayLocalResults);
}

function displayLocalResults (weather) {
  let city = document.querySelector('.current-location .current-city');
  city.innerText = `${weather.name}, ${weather.sys.country}`;

  let now = new Date();
  let date = document.querySelector('.current-location .date');
  date.innerText = dateBuilder(now);
  
  let temp = document.querySelector('.current .local-temp');
  temp.innerHTML = `${Math.round(weather.main.temp)}<span>°C</span>`;

  let weather_el = document.querySelector('.current .local-weather');
  weather_el.innerText = weather.weather[0].main;

  let iconcode = weather.weather[0].icon;
  let iconurl = "https://openweathermap.org/img/wn/" + iconcode + "@2x.png";
  $('#lwicon').attr('src', iconurl);

  let hilow = document.querySelector('.local-hi-low');
  hilow.innerText = `${Math.round(weather.main.temp_min)}°C / ${Math.round(weather.main.temp_max)}°C`;
}

function setQuery(evt) {
  if (evt.keyCode == 13) {
    getResults(searchbox.value);
  }
}

function getResults (query) {
  fetch(`${api.base}weather?q=${query}&units=metric&APPID=${api.key}`)
  .then(weather => {
    return weather.json();
  }).then(displayResults);
}

function displayResults (weather) {
  let city = document.querySelector('.location .city');
  city.innerText = `${weather.name}, ${weather.sys.country}`;
  
  let timestamp = (weather.dt+weather.timezone)*1000;
  let now = new Date(timestamp);
  let date = document.querySelector('.location .date');
  date.innerText = dateBuilder(now);
  timestamp = 0;

  let temp = document.querySelector('.current .temp');
  temp.innerHTML = `${Math.round(weather.main.temp)}<span>°C</span>`;

  let weather_el = document.querySelector('.current .weather');
  weather_el.innerText = weather.weather[0].main;

  let iconcode = weather.weather[0].icon;
  let iconurl = "http://openweathermap.org/img/wn/" + iconcode + "@2x.png";
  $('#wicon').attr('src', iconurl);

  let hilow = document.querySelector('.hi-low');
  hilow.innerText = `${Math.round(weather.main.temp_min)}°C / ${Math.round(weather.main.temp_max)}°C`;
}

function dateBuilder (d) {
  let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "Noverber", "December"];
  let days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

  let day = days[d.getDay()];
  let date = d.getDate();
  let month = months[d.getMonth()];
  let year = d.getFullYear();

  return `${day} ${date} ${month} ${year}`
}